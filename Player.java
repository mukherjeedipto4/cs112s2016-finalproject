public class Player {
	private int score;
	private int phValue;

	public Player() {
		score = 50;
		phValue=0;
	}

	public void setScore(int newScore) {
		score = newScore;
	}
	public void setPhValue(int newPhValue) {
		phValue = newPhValue;
	}
	public int getScore() {
		return score;
	}
	public int getphValue() {
		return phValue;
	}

}
