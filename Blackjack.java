import java.util.*;
public class Blackjack {
    private static ArrayList<Card> playerHand = new ArrayList<Card>();
    private static ArrayList<Card> houseHand = new ArrayList<Card>();
    private static int houseHandValue;
    private static Stack<Card> gameDeck;
    private static Player player = new Player();
    private static boolean badInput;
    public static void main(String [] args) {
        Scanner scan = new Scanner(System.in);
        String userInput;
        String playAgain;
        int wager;
        int newScore;
        System.out.println("Welcome to the BBM Blackjack \nIt's only a gambling problem if you lose.");
        do {
            Deck deck = new Deck();
            System.out.println("How much would you like to wager? Table min:5 Table max: 50");
            wager = scan.nextInt();
            if (wager > 50 || wager > player.getScore() || wager < 5){
                System.out.println("You bet an invalid amount.");
                System.out.println("Using default bet; table min of 5.");
                wager = 5;
            }
            gameDeck = deck.getShuffledDeck();
            deal();
            if (player.getphValue() == 21){
                player.setScore(player.getScore() + wager);
                System.out.println("Your score is " + player.getScore());
                System.out.println("Would you like to play again?");
                playAgain = scan.next();
                continue;
            }
            System.out.println("Would you like to hit? 'YES' or 'NO'");
            userInput = scan.next();
            do {
                if (userInput.equalsIgnoreCase("YES")){
                    hit();
                    for (int i = 0; i < playerHand.size(); i++){
                        System.out.print(playerHand.get(i).getValue() + "-" + playerHand.get(i).getSuit() + " ");
                    }//for
                    if (bust(player.getphValue())){
                        System.out.println("Player value: "+player.getphValue());
                        /* System.out.println("You lose!");
                           newScore = player.getScore() - wager;
                           player.setScore(newScore);
                           System.out.println("You lost " + wager + " your new score is " + player.getScore());*/
                        break;
                    } else {
                        System.out.print("Player value: ");
                        System.out.println(player.getphValue());
                        System.out.println("Would you like to hit again?");
                        userInput = scan.next();
                    }//else if
                } else if(userInput.equalsIgnoreCase("NO")){
                    houseHit();
                    break;
                } else {
                    System.out.println("You did not type 'YES' or 'NO'");
                }//else if
            } while (userInput.equalsIgnoreCase("YES") || userInput.equalsIgnoreCase("NO"));
            //System.out.print(playerHand.get(0).getValue() + "-" + playerHand.get(1).getSuit() + " " + playerHand.get(1).getValue() + "-" + playerHand.get(1).getSuit() + " " + playerHand.get(2).getValue() + "-" + playerHand.get(2).getSuit()+" ");
            // winner();
            if (winner() == true){
                newScore = player.getScore() + wager;
                player.setScore(newScore);
                System.out.println("You have won the hand!");
                System.out.println("You won " + wager + " your new score is " + player.getScore());

            } else {
                newScore = player.getScore() - wager;
                player.setScore(newScore);
                System.out.println("Sorry, you have lost this hand.");
                System.out.println("You lost " + wager + " your new score is " + player.getScore());


            }
            do {
                System.out.println("Do you want to play again? Yes/No");
                playAgain = scan.next();
                if (playAgain.equalsIgnoreCase("YES")){
                    emptyHands();
                    badInput = false;
                    continue;
                } else if (playAgain.equalsIgnoreCase("NO")){
                    badInput = false;
                    continue;
                } else
                    System.out.println("You entered bad Input. \nPlease try again.");
                    badInput = true;
            }while (badInput);
        }while(player.getScore() > 0 && playAgain.equalsIgnoreCase("Yes"));
        if (playAgain.equalsIgnoreCase("NO") || !playAgain.equalsIgnoreCase("Yes")){
            System.out.println("You left the table. \n Your final score is: " + player.getScore());
        } else {
            System.out.println("You ran out of score. \n You lose \n If you have gambling problems please call 1-800-522-4700");
        }
    }

    public static void deal() {
        playerHand.add(gameDeck.pop());
        playerHand.add(gameDeck.pop());
        houseHand.add(gameDeck.pop());
        houseHand.add(gameDeck.pop());
        System.out.print("Player hand: ");
        System.out.print(playerHand.get(0).getValue() + "-" + playerHand.get(0).getSuit()+" ");
        System.out.print(playerHand.get(1).getValue() + "-" + playerHand.get(1).getSuit()+" ");
        System.out.print("Player value: ");
        player.setPhValue(convert(playerHand.get(0).getValue()) + convert(playerHand.get(1).getValue()));
        if(player.getphValue() > 21)
            player.setPhValue(12);
        else if(player.getphValue() == 21){
            System.out.println("Winner winner chicken dinner!!!!!");
            emptyHands();
        }
        System.out.print(player.getphValue());
        System.out.println();
        System.out.print("House card: ");
        System.out.print(houseHand.get(1).getValue() + "-" + houseHand.get(1).getSuit()+" ");
        System.out.println();
    }

    private static int convert(String value) {
        int convertedValue = 0;
        switch(value) {
            case "2" :
                convertedValue = 2;
                break;
            case "3" :
                convertedValue = 3;
                break;
            case "4" :
                convertedValue = 4;
                break;
            case "5" :
                convertedValue = 5;
                break;
            case "6" :
                convertedValue = 6;
                break;
            case "7" :
                convertedValue = 7;
                break;
            case "8" :
                convertedValue = 8;
                break;
            case "9" :
                convertedValue = 9;
                break;
            case "10" :
            case "Jack" :
            case "Queen" :
            case "King" :
                convertedValue = 10;
                break;
            case "Ace" :
                convertedValue = 11;
                break;
        }
        return convertedValue;

    }//convert

    public static void hit(){
        int value = 0;
        playerHand.add(gameDeck.pop());
        System.out.print("Player hand: ");
        for (int i = 0; i < playerHand.size(); i++){
            player.setPhValue(convert(playerHand.get(i).getValue()));
            value += player.getphValue();
        }//for
        if (value > 21){
            for (int i = 0; i < playerHand.size(); i++){
                if (playerHand.get(i).getValue().equalsIgnoreCase("ACE")){
                    value -= 10;
                }//if
            }//for
        }//if
        player.setPhValue(value);
    }//hit

    public static boolean bust(int phValue){
        if (phValue > 21){
            return true;
        }else
            return false;
    }//bust

    public static void houseHit(){
        /*for (int i = 0; i < houseHand.size(); i++) {
          System.out.print(houseHand.get(i).getValue() + "-" + houseHand.get(i).getSuit()+" ");
        }//for*/
        for (int i = 0; i < houseHand.size(); i++) {
            houseHandValue += convert(houseHand.get(i).getValue());
        }
        while (houseHandValue < 17 && houseHandValue <= player.getphValue()){
            houseHand.add(gameDeck.pop());
            houseHandValue = 0;
            for (int i = 0; i < houseHand.size(); i++){
                houseHandValue += convert(houseHand.get(i).getValue());
            }//for
        }//while
        System.out.print("House hand: ");
        for (int i = 0; i < houseHand.size(); i++) {
            System.out.print(houseHand.get(i).getValue() + "-" + houseHand.get(i).getSuit()+" ");
            System.out.println();
        }//for
        if (houseHandValue > 21){
            for (int i = 0; i < houseHand.size(); i++){
                if (houseHand.get(i).getValue().equalsIgnoreCase("ACE")){
                    houseHandValue -= 10;
                }//if
            }//for
        }//if
        System.out.print("House value: ");
        System.out.println(houseHandValue);
    }//houseHit

    public static boolean winner(){
        if ((houseHandValue > player.getphValue() && houseHandValue < 22) || bust(player.getphValue())){
            return false;
        }else {
            return true;

        }
    }
    public static void emptyHands() {
        /*for(int i =0;i<playerHand.size();i++) {
          playerHand.set(i,null);
        }
        for(int i =0;i<houseHand.size();i++) {
        houseHand.set(i,null);
        }*/
        playerHand.clear();
        houseHand.clear();
        player.setPhValue(0);
        houseHandValue = 0;
    }
}
