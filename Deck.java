import java.util.ArrayList;
import java.util.Iterator;
import java.util.*;
public class Deck {
	private String [] suits = {"Spades","Clubs","Hearts","Diamonds"};
	private String [] values = {"2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"}; 
	private ArrayList<Card> deckOfCards;
	private Stack<Card> deck;
	public Deck() {
		deck = new Stack<Card>();
		deckOfCards = new ArrayList<Card>();
		for(int i =0;i<suits.length;i++) {
			for(int j=0;j<values.length;j++) {
				Card thisCard = new Card(suits[i],values[j]);
				deckOfCards.add(thisCard);
			}
		}

	}
	public void printDeck() {
		Iterator<Card> it = deckOfCards.iterator();
		while(it.hasNext()) {
			Card thisCard = it.next();
			System.out.print(thisCard.getValue() + "-" +thisCard.getSuit()+" ");
		}

	}
	public void shuffle() {
		Random rand = new Random();
		Iterator<Card> it = deckOfCards.iterator();
		Card [] randomDeck = new Card[52];
		int counter = 1;
		int hashValue = rand.nextInt(52);
		for(int i=0;i<52;i++) {
			if(randomDeck[hashValue] != null) {
				counter = 1;
				while(counter < 52) {
					if(randomDeck[(hashValue + counter)%52] == null) {
						randomDeck[(hashValue + counter)%52] = it.next();
						break;
					}
					counter++;
				}
				if(counter == 52) {
					System.out.println("no more space!");
					//return;
				}
			}
			else {
				randomDeck[hashValue] = it.next();
			}
			hashValue = rand.nextInt(52);
		}
		for(int i =0;i<randomDeck.length;i++) {
			deck.push(randomDeck[i]);
		}
		/*for(int i=0;i<52;i++) {
			Card thisCard = deck.pop();
			System.out.print(thisCard.getValue()+"-"+thisCard.getSuit()+" ");
		}
		System.out.println();*/

	}
	public Stack<Card> getShuffledDeck() {
		shuffle();
		return deck;
	}

}


